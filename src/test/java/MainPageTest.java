import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class MainPageTest {

    private WebDriver driver;
    private MainPage mainPage;
    private WebDriverWait wait;


    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com");
        mainPage = new MainPage(driver);
    }

    @Test
    public void clickLogoTest(){
        mainPage.clickOnLogo();
        String title = driver.getTitle();
        Assert.assertEquals("My Store",title);
    }

    @Test
    public void clickSignInTest(){
        LoginPage loginPage = mainPage.clickOnSignInButton();
        String heading = loginPage.getSignInHeading();
        Assert.assertEquals("AUTHENTICATION", heading);
    }

    @Test
    public void clickContactUsTest(){
        ContactPage contactPage = mainPage.clickOnContactUsButton();
        String contactHeading = contactPage.getContactHeadingText();
        Assert.assertEquals("CUSTOMER SERVICE - CONTACT US",contactHeading);

    }

    @Test
    public void clickCartButtonTest(){
        CartPage cartPage = mainPage.clickOnCartButton();
        String cartPageHeadingText = cartPage.getCartPageHeadingText();
        Assert.assertEquals("SHOPPING-CART SUMMARY",cartPageHeadingText);

    }





    @After
    public void closeBrowser(){
        driver.quit();
    }








}

