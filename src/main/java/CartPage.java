import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {

    private WebDriver driver;

    public CartPage (WebDriver driver){
        this.driver = driver;
    }

    private By cartPageHeading = By.xpath("//*[@id=\"cart_title\"]");


    public String getCartPageHeadingText(){
        return driver.findElement(cartPageHeading).getText();
    }

}

