import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactPage {

    private WebDriver driver;

    public ContactPage (WebDriver driver){
        this.driver = driver;
    }

    private By contactHeading = By.xpath("//*[@id=\"center_column\"]/h1");

    public String getContactHeadingText(){
        return driver.findElement(contactHeading).getText();
    }
}
