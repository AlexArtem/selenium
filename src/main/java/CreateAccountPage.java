import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.Assert;



public class CreateAccountPage {
    public static final String USER_NAME = "Alex";

    WebDriver driver;

    public CreateAccountPage(WebDriver driver) {

        this.driver = driver;
    }

    private By mrRadioButton = By.xpath("//*[@id=\"id_gender1\"]");

    private By mrsRadioButton = By.xpath("//*[@id=\"id_gender2\"]");

    private By firstName = By.xpath("//*[@id=\"customer_firstname\"]");

    private By lastName = By.xpath("//*[@id=\"customer_lastname\"]");

    private By email = By.xpath("//*[@id=\"email\"]");

    private By password = By.xpath("//*[@id=\"passwd\"]");

    private By dayOfBirth = By.xpath("//*[@id=\"days\"]");

    private By monthOfBirth = By.xpath("//*[@id=\"months\"]");

    private By yearOfBirth = By.xpath("//*[@id=\"years\"]");

    private By signUpNewsletterCheckbox = By.xpath("//*[@id=\"newsletter\"]");

    private By receiveSpecialOffersCheckbox = By.xpath("//*[@id=\"optin\"]");

    private By addressFirstName = By.xpath("//*[@id=\"firstname\"]");

    private By addressLastName = By.xpath("//*[@id=\"lastname\"]");

    private By company = By.xpath("//*[@id=\"company\"]");

    private By address = By.xpath("//*[@id=\"address1\"]");

    private By addressLine2 = By.xpath("//*[@id=\"address2\"]");

    private By city = By.xpath("//*[@id=\"city\"]");

    private By state = By.xpath("//*[@id=\"id_state\"]");

    private By zipPostalCode = By.xpath("//*[@id=\"postcode\"]");

    private By country = By.xpath("//*[@id=\"id_country\"]");

    private By additionalInfoField = By.xpath("//*[@id=\"other\"]");

    private By homePhone = By.xpath("//*[@id=\"phone\"]");

    private By mobilePhone = By.xpath("//*[@id=\"phone_mobile\"]");

    private By addressAlias = By.xpath("//*[@id=\"alias\"]");

    private By submitRegistrationButton = By.xpath("//*[@id=\"submitAccount\"]");


    public CreateAccountPage mrGenderCheck(){
        driver.findElement(mrRadioButton).click();
        return this;
    }

    public CreateAccountPage mrsGenderCheck(){
        driver.findElement(mrsRadioButton).click();
        return this;
    }

    public CreateAccountPage firstNameFill(){
        driver.findElement(firstName).sendKeys("Alex");
        return this;
    }

    public String firstNameAutoFillText(){
        String firstNameAutofilledText = driver.findElement(firstName).getText();
        return firstNameAutofilledText;
    }






    public CreateAccountPage lastNameFill(){
        driver.findElement(lastName).sendKeys("Artem");
        return this;
    }

    public String lastNameAutoFillText(){
        String lastNameAutofilledText = driver.findElement(lastName).getText();
        return lastNameAutofilledText;
    }

    public CreateAccountPage emailAutoFill(){
        driver.findElement(email);
        return this;

    }

    public String emailAutofillGetText(){
        String emailText = driver.findElement(email).getAttribute("value");
        return emailText;

    }

    public CreateAccountPage typePassword(){
        driver.findElement(password).sendKeys(MainClass.password);
        return this;

    }

    public CreateAccountPage typeDayOfBirth(){
        driver.findElement(dayOfBirth).sendKeys("31");
        return this;

    }

    public CreateAccountPage typeMonthOfBirth(){
        driver.findElement(monthOfBirth).sendKeys("december");
        return this;

    }

    public CreateAccountPage typeYearOfBirth(){
        driver.findElement(yearOfBirth).sendKeys("1988");
        return this;

    }

    public CreateAccountPage enableSignUpNewsCheckbox(){
        driver.findElement(signUpNewsletterCheckbox).click();
        return this;

    }

    public CreateAccountPage enableSpecialOffersCheckbox(){
        driver.findElement(receiveSpecialOffersCheckbox).click();
        return this;

    }

    public CreateAccountPage addressFirstName(){
        driver.findElement(addressFirstName);
        return this;

    }

    public CreateAccountPage addressLastName(){
        driver.findElement(addressLastName);
        return this;

    }
    public String getAddressFirstName(){
        String autofilledAddressFirstName = driver.findElement(addressFirstName).getText();
        return autofilledAddressFirstName;

    }

    public String getAddressLastName(){
        String autofilledAddressFLastName = driver.findElement(addressLastName).getText();
        return autofilledAddressFLastName;

    }





    public CreateAccountPage fillCompany(){
        driver.findElement(company).sendKeys("CTDEV");
        return this;

    }

    public CreateAccountPage fillCity(){
        driver.findElement(city).sendKeys("Kyiv");
        return this;

    }

    public CreateAccountPage fillAddress(){
        driver.findElement(address).sendKeys("Super Secret Address");
        return this;

    }

    public CreateAccountPage fillAddressLine2(){
        driver.findElement(addressLine2).sendKeys("Everywhere");
        return this;
    }

    public CreateAccountPage fillState(){
        driver.findElement(state).sendKeys("Arizona");
        return this;
    }

    public CreateAccountPage fillZipPostalCode(){
        driver.findElement(zipPostalCode).sendKeys("00000");
        return this;
    }

    public CreateAccountPage fillCountry(){
        driver.findElement(country).sendKeys("United States");
        return this;
    }

    public CreateAccountPage fillAdditionalInfoField(){
        driver.findElement(additionalInfoField).sendKeys("Some Text");
        return this;
    }

    public CreateAccountPage typeHomePhone(){
        driver.findElement(homePhone).sendKeys("1234567890");
        return this;
    }

    public CreateAccountPage typeMobilePhone(){
        driver.findElement(mobilePhone).sendKeys("+380937583374");
        return this;
    }

    public CreateAccountPage typeAddressAlias(){
        driver.findElement(addressAlias).sendKeys("Some Address Alias");
        return this;
    }

    public CreateAccountPage submitButtonClick(){
        driver.findElement(submitRegistrationButton).click();
        return new CreateAccountPage(driver);
    }























}
