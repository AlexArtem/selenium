import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {

    //WebDriver driver;

    public LoginPage(WebDriver driver) {
        super(driver);
//       // this.driver=driver;

    }

    private By emailAddressCreate = By.xpath("//*[@id=\"email_create\"]");

    private By alreadyRegisteredMail = By.xpath("//*[@id=\"email\"]");

    private By alreadyRegisteredPassword = By.xpath("//*[@id=\"passwd\"]");

    private By alreadyRegisteredSignInButton = By.xpath("//*[@id=\"SubmitLogin\"]");

    private By createAccountButton = By.xpath("//*[@id=\"SubmitCreate\"]");

    private By signInPageHeading = By.xpath("//*[@id=\"center_column\"]/h1");

    private By invalidEmailError = By.xpath("//*[@id=\"create_account_error\"]/ol/li");




    public LoginPage newEmailAddressType(String generatedMail) {
        driver.findElement(emailAddressCreate).sendKeys(generatedMail);
        return this;

    }


    public LoginPage createAccountSubmit(){
        driver.findElement(createAccountButton).click();
        return new LoginPage(driver);
    }


    public LoginPage alreadyRegisteredMailType(String registeredMail) {
        driver.findElement(alreadyRegisteredMail).sendKeys();
        return this;

    }

    public MainPage login(String username, String password) {
        return null;
    }



    public LoginPage alreadyRegisteredPasswordType(String password) {
        driver.findElement(alreadyRegisteredPassword).sendKeys("abrakadabra");
        return this;

    }

    public String getSignInHeading(){
        return driver.findElement(signInPageHeading).getText();
    }

    public String invalidEmailErrorGet() {

        return driver.findElement(invalidEmailError).getText();

    }



}

