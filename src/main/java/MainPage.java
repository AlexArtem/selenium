import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage {

    private WebDriver driver;

    public MainPage (WebDriver driver){
        this.driver = driver;
    }


    private By logo = By.xpath("//*[@id=\"header_logo\"]/a/img");

    private By signInButton = By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a");

    private By contactUsButton = By.xpath("//*[@id=\"contact-link\"]/a");

    private String mainPageTitleText = ("My Store");

    private  By cartButton = By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a");





    public MainPage clickOnLogo(){
        driver.findElement(logo).click();
        return this;

    }


    public LoginPage clickOnSignInButton() {
        driver.findElement(signInButton).click();
        return new LoginPage(driver);

    }

    public ContactPage clickOnContactUsButton(){
        driver.findElement(contactUsButton).click();
        return new ContactPage(driver);
    }

    public CartPage clickOnCartButton(){
        driver.findElement(cartButton).click();
        return new CartPage(driver);
    }











}
