import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class CreateAccountTest {

    private WebDriver driver;
    private LoginPage loginPage;
    private WebDriverWait wait;
    private CreateAccountPage createAccountPage;




    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/Users/oleksandrartemenko/Downloads/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
        loginPage = new LoginPage(driver);



    }

    @Test
    public void createAccountEmptyMail() {
        loginPage.newEmailAddressType(" ").createAccountSubmit();
        String invalidEmailErrorText = loginPage.invalidEmailErrorGet();
        Assert.assertEquals("Invalid email address.",invalidEmailErrorText);


    }
    @Test
    public void createAccountInvalidMail() {
        loginPage.newEmailAddressType("alexartem_gmail.com ").createAccountSubmit();
        String invalidEmailErrorText = loginPage.invalidEmailErrorGet();
        Assert.assertEquals("Invalid email address.",invalidEmailErrorText);


    }

    @Test
    public void createAccountValidMail() {
        loginPage.newEmailAddressType(MainClass.generatedMail)
                .createAccountSubmit();

        CreateAccountPage createAccountPage = new CreateAccountPage(driver);

        createAccountPage
                .mrGenderCheck()
                .mrsGenderCheck()
                .firstNameFill()
                .lastNameFill();

        String autoFilledEmail = createAccountPage.emailAutofillGetText();
        Assert.assertEquals(MainClass.generatedMail,autoFilledEmail);

        createAccountPage
                .typePassword()
                .typeDayOfBirth()
                .typeMonthOfBirth()
                .typeYearOfBirth()
                .enableSignUpNewsCheckbox()
                .enableSpecialOffersCheckbox();


        String addressFirstName = createAccountPage.getAddressFirstName();
        Assert.assertEquals(createAccountPage.firstNameAutoFillText(),addressFirstName);



        String addressLastName = createAccountPage.getAddressLastName();
        Assert.assertEquals(createAccountPage.lastNameAutoFillText(),addressLastName);


        createAccountPage
                .fillCompany()
                .fillAddress()
                .fillAddressLine2()
                .fillCity()
                .fillState()
                .fillZipPostalCode()
                .fillCountry()
                .fillAdditionalInfoField()
                .typeHomePhone()
                .typeMobilePhone()
                .typeAddressAlias()
                .submitButtonClick();

        MyAccountPage myAccountPage = new MyAccountPage(driver);
        Assert.assertEquals(myAccountPage.myAccountPageHeadingText(),"MY ACCOUNT");
        Assert.assertEquals(driver.getTitle(),"My account - My Store");
        Assert.assertEquals(myAccountPage.myAccountNameShowing(), MainClass.username);


    }





    @After
    public void closeBrowser() {
        driver.quit();
    }






}
