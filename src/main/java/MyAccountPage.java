import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyAccountPage {

    private WebDriver driver;

    public MyAccountPage(WebDriver driver){
        this.driver=driver;
    }

    private By myAccountPageHeading = By.xpath("//*[@id=\"center_column\"]/h1");

    private  By accountName = By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a/span");

    public String myAccountPageHeadingText(){
        String text = driver.findElement(myAccountPageHeading).getText();
        return text;
    }

    public String myAccountNameShowing(){
        String myAccountNameText = driver.findElement(accountName).getText();
        return myAccountNameText;
    }




}
